const getDadJoke = async () => {
  const res = await fetch('/joke', { headers: { "Accept": "application/json" }}) 
  const { joke } = await res.json()
  document.getElementById('dad-joke').innerText = joke
}

const getWord = async () => {
  const res = await fetch('/api/getWord', { headers: { "Accept": "application/json" } }) 
  const word = await res.text()  
  document.getElementById('random-word').innerText = word
} 

getDadJoke()
getWord()